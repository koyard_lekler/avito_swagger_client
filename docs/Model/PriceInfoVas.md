# PriceInfoVas

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**premium** | **int** | Стоимость услуги \&quot;Премиум\&quot; | [optional] 
**vip** | **int** | Стоимость услуги \&quot;VIP\&quot; | [optional] 
**pushup** | **int** | Стоимость услуги \&quot;Поднять\&quot; | [optional] 
**highlight** | **int** | Стоимость услуги \&quot;Выделить\&quot; | [optional] 
**xl** | **int** | Стоимость услуги \&quot;XL-объявление\&quot; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

