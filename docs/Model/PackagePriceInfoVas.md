# PackagePriceInfoVas

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quick_sale** | **int** | Стоимость пакета услуг \&quot;Быстрая продажа\&quot; | [optional] 
**turbo_sale** | **int** | Стоимость пакета услуг \&quot;Турбо-продажа\&quot; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

